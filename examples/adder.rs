use actorize::*;

struct Add(usize);

struct Adder {
    sum: usize,
}

impl Message for Add {}
impl Actor for Adder {}

impl Handles<Add> for Adder {
    fn handle(&mut self, msg: Add, _ctx: &Context<Self>) {
        self.sum += msg.0;
        println!("Sum is now: {}", self.sum);
    }
}

#[tokio::main]
async fn main() {
    let mut sys = ActorSystem::new();

    {
        let adder = sys.start_actor(Adder { sum: 0 });

        adder.send(Add(5));
        adder.send(Add(10));
    }

    sys.broadcast_shutdown();
    sys.wait_for_completion().await;
}
