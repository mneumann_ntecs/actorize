use actorize::*;

/// -----------------------
/// Messages
/// -----------------------

struct Clear;
struct Add(isize);
struct Sub(isize);

struct QueryResult<A: Handles<CalculatorResult>> {
    reply: Addr<A>,
}

struct CalculatorResult(isize);

// Tag all types as messages
//
impl Message for Clear {}
impl Message for Add {}
impl Message for Sub {}
impl<A: Handles<CalculatorResult>> Message for QueryResult<A> {}
impl Message for CalculatorResult {}

/// -----------------------
/// Calculator actor
/// -----------------------

struct Calculator {
    result: isize,
}

impl Actor for Calculator {}

impl Handles<Clear> for Calculator {
    fn handle(&mut self, _msg: Clear, _ctx: &Context<Self>) {
        self.result = 0;
    }
}

impl Handles<Add> for Calculator {
    fn handle(&mut self, Add(add): Add, _ctx: &Context<Self>) {
        self.result += add;
    }
}

impl Handles<Sub> for Calculator {
    fn handle(&mut self, Sub(sub): Sub, _ctx: &Context<Self>) {
        self.result -= sub;
    }
}

impl<A: Handles<CalculatorResult>> Handles<QueryResult<A>> for Calculator {
    fn handle(&mut self, QueryResult { reply }: QueryResult<A>, _ctx: &Context<Self>) {
        reply.send(CalculatorResult(self.result));
    }
}

/// -----------------------
/// Reporter actor
/// -----------------------

struct Reporter;
impl Actor for Reporter {}

impl Handles<CalculatorResult> for Reporter {
    fn handle(&mut self, CalculatorResult(result): CalculatorResult, _ctx: &Context<Self>) {
        println!("Reporter: The result is: {}", result);
    }
}

/// -----------------------
/// Main actor
/// -----------------------

struct Main {
    reporter: Addr<Reporter>,
    calc: Addr<Calculator>,
}

impl Actor for Main {
    fn started(&mut self, context: &Context<Self>) {
        self.calc.send(Clear);

        // This should print "0"
        self.calc.send(QueryResult {
            reply: self.reporter.clone(),
        });

        for i in 1..=100 {
            self.calc.send(Add(i));
        }

        // This should print "5050"
        self.calc.send(QueryResult {
            reply: self.reporter.clone(),
        });

        for i in 1..=100 {
            self.calc.send(Sub(i));
        }

        // This should print "0"
        self.calc.send(QueryResult {
            reply: self.reporter.clone(),
        });

        // This should print "0"
        self.calc.send(QueryResult {
            reply: context.myself(),
        });
    }
}

impl Handles<CalculatorResult> for Main {
    fn handle(&mut self, CalculatorResult(result): CalculatorResult, _ctx: &Context<Self>) {
        println!("Main: The result is: {}", result);
    }
}

/// -----------------------
/// main
/// -----------------------

#[tokio::main]
async fn main() {
    let mut sys = ActorSystem::new();

    {
        let calc = sys.start_actor(Calculator { result: 0 });
        let reporter = sys.start_actor(Reporter);
        let _main = sys.start_actor(Main { reporter, calc });
    }

    sys.wait_for_completion().await;
}
