# actorize

Actor framework for Rust, modelled after [Pony][Pony]'s actors:

* `Actor`s cannot deadlock!

* `Send`ing a message to an `Actor` can never fail.

* No `async` in `Actor`s. Behaviors cannot block.

* `Actor`s are garbage collected.

* The implemention of `actorize` is rather simple compared against other Actor
  implementations in Rust.

[Pony]: https://www.ponylang.io/
