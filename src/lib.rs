use flume::*;
use std::collections::hash_map::HashMap;
use tokio::task::{self, JoinHandle};

/// Any message that can be sent between actors.
pub trait Message: Send + 'static {}

/// An actor.
pub trait Actor: Send + Sized + 'static {
    /// Called after the Actor was started.
    fn started(&mut self, _context: &Context<Self>) {}

    /// Called before the Actor is stopped.
    fn stopped(&mut self) {}
}

/// Message handler.
pub trait Handles<T: Message>
where
    Self: Actor,
{
    fn handle(&mut self, message: T, context: &Context<Self>);
}

/// The type of messages that we pass between actors.
enum ActorMessage<A: Actor> {
    /// Handle a user defined message.
    HandleMessage(Box<dyn FnOnce(&mut A, &Context<A>) + Send + 'static>),

    /// Increase the actors reference count.
    RefActor,

    /// Decrease the actors reference count.
    UnRefActor,

    /// The system is shutting down. Messages following this message will not be handled.
    SysShutdown,
}

struct Mailbox<A: Actor> {
    receiver: Receiver<ActorMessage<A>>,
    fully_consumed: bool,
}

#[inline]
fn send_or_panic<M>(sender: &Sender<M>, message: M) {
    match sender.send(message) {
        Ok(()) => {}
        Err(_) => panic!("FATAL - Channel closed unexpectedly"),
    }
}

/// Address of an Actor. Required to send a message to it.
pub struct Addr<A: Actor> {
    sender: Sender<ActorMessage<A>>,
}

impl<A: Actor> Clone for Addr<A> {
    fn clone(&self) -> Self {
        send_or_panic(&self.sender, ActorMessage::RefActor);
        Self {
            sender: self.sender.clone(),
        }
    }
}

impl<A: Actor> Drop for Addr<A> {
    fn drop(&mut self) {
        send_or_panic(&self.sender, ActorMessage::UnRefActor);
    }
}

impl<A: Actor> Addr<A> {
    /// Send `message` to the actor represented by this address.
    ///
    /// Sending can never fail!
    pub fn send<M: Message>(&self, message: M)
    where
        A: Handles<M>,
    {
        let actor_message = ActorMessage::HandleMessage(Box::new(move |actor, context| {
            Handles::<M>::handle(actor, message, context)
        }));
        send_or_panic(&self.sender, actor_message);
    }
}

pub struct Context<A: Actor> {
    myself: Addr<A>,
}

impl<A: Actor> Context<A> {
    pub fn myself(&self) -> Addr<A> {
        self.myself.clone()
    }

    pub fn myself_ref(&self) -> &Addr<A> {
        &self.myself
    }
}

async fn actor_main<A: Actor>(actor: &mut A, mailbox: &mut Mailbox<A>, context: Context<A>) {
    let () = actor.started(&context);
    let () = handle_messages(actor, mailbox, context).await;
    let () = actor.stopped();
}

async fn handle_messages<A: Actor>(actor: &mut A, mailbox: &mut Mailbox<A>, context: Context<A>) {
    let mut ref_cnt: isize = 0;

    let mut next_msg = mailbox.receiver.recv_async().await.ok();

    loop {
        match next_msg.take() {
            Some(ActorMessage::HandleMessage(handler)) => handler(actor, &context),
            Some(ActorMessage::RefActor) => {
                ref_cnt += 1;
            }
            Some(ActorMessage::UnRefActor) => {
                debug_assert!(ref_cnt > 0);
                ref_cnt -= 1;
            }
            Some(ActorMessage::SysShutdown) => {
                break;
            }
            None => {
                if ref_cnt == 0 {
                    mailbox.fully_consumed = true;
                    break;
                } else {
                    debug_assert!(ref_cnt > 0);
                    panic!("Mailbox was closed prematurely");
                }
            }
        }

        next_msg = if ref_cnt > 0 {
            mailbox.receiver.recv_async().await.ok()
        } else {
            // In case there are no further references to this actor, we still want to
            // process all remaining messages in the mailbox. Processing these messages
            // might again create new references to the actor.
            //
            // Use non-blocking receive.
            mailbox.receiver.try_recv().ok()
        };
    }

    // This should still send out a last ActorUnRef msg.
    // We don't care about this.
    drop(context);
}

type ActorId = usize;

struct ActorTermination {
    send_shutdown: Option<Box<dyn FnOnce() + Send + 'static>>,
    join_handle: JoinHandle<()>,
}

/// Messages sent to the actor system.
#[derive(Debug)]
enum ActorSystemMessage {
    ActorTerminated(ActorId),
}

pub struct ActorSystem {
    next_actor_id: ActorId,
    actor_system_sender: Sender<ActorSystemMessage>,
    actor_system_receiver: Receiver<ActorSystemMessage>,
    actors: HashMap<ActorId, ActorTermination>,
}

impl ActorSystem {
    pub fn new() -> Self {
        let (actor_system_sender, actor_system_receiver) = unbounded();
        Self {
            next_actor_id: 0,
            actor_system_sender,
            actor_system_receiver,
            actors: HashMap::new(),
        }
    }

    /// Starts the actor in the actor system.
    pub fn start_actor<A: Actor>(&mut self, actor: A) -> Addr<A> {
        let actor_id = self.next_actor_id;

        self.next_actor_id += 1;

        let (sender, receiver) = unbounded::<ActorMessage<A>>();

        let addr = Addr {
            sender: sender.clone(),
        };

        let context = Context {
            myself: addr.clone(),
        };

        let mailbox = Mailbox {
            receiver,
            fully_consumed: false,
        };

        // The actor can use call into the actor system. For example to inform the
        // actor system that an actor has terminated.
        let actor_system_channel = self.actor_system_sender.clone();

        let join_handle = task::spawn(async move {
            let actor_system_channel = actor_system_channel;
            let mut actor = actor;
            let mut mailbox = mailbox;
            let context = context;

            actor_main(&mut actor, &mut mailbox, context).await;

            drop(actor);

            // Inform the actor system that this actor has terminated.
            // This allows the actor system maintenance task to drop it from the registry.
            println!("Actor {} ready for reaping", actor_id);

            send_or_panic(
                &actor_system_channel,
                ActorSystemMessage::ActorTerminated(actor_id),
            );
            drop(actor_system_channel);
        });

        // The actor system has an additional reference to the channel.
        let sender2 = sender.clone();

        let actor_termination = ActorTermination {
            send_shutdown: Some(Box::new(move || {
                send_or_panic(&sender2, ActorMessage::SysShutdown)
            })),
            join_handle,
        };

        if let Some(_conflict) = self.actors.insert(actor_id, actor_termination) {
            panic!("Duplicate actor in registry");
        }

        addr
    }

    pub fn broadcast_shutdown(&mut self) {
        for (_k, actor_termination) in self.actors.iter_mut() {
            if let Some(send_shutdown) = actor_termination.send_shutdown.take() {
                send_shutdown();
            }
        }
    }

    pub async fn wait_for_completion(self) {
        let ActorSystem {
            actor_system_receiver,
            actor_system_sender,
            mut actors,
            ..
        } = self;
        drop(actor_system_sender);

        while let Some(actor_system_message) = actor_system_receiver.recv_async().await.ok() {
            match actor_system_message {
                ActorSystemMessage::ActorTerminated(actor_id) => match actors.remove(&actor_id) {
                    Some(ActorTermination { join_handle, .. }) => match join_handle.await {
                        Ok(()) => {}
                        Err(join_error) => panic!(
                            "Termination of actor #{} failed: {:?}",
                            actor_id, join_error
                        ),
                    },
                    None => {
                        panic!(
                            "Got ActorTerminated message for unknown actor #{}",
                            actor_id
                        );
                    }
                },
            }
        }
        drop(actor_system_receiver);
        if actors.len() > 0 {
            panic!("Actor system termination failed");
        }
    }
}
